# GitLab suggestions

## Prompts

- Write a comment
- Press enter
- Tab to accept

### Simple

```text
// Generate a function to add two numbers
// javascript: Generate a function to add two numbers
// golang: Generate a function to add two numbers
```

### Complex

```text
// golang: Generate a small http server to display an html message: hello world

// golang: generate a cli program using the Flags package with a string parameter --name and prints the value of name
```

### Some don't work

// Create a function to generate an unique id
// Generate a function to ruturn an unique id
// Generate a function to compute an unique string id
// Generate a function that returns an unique string id

### Other prompts
// Generate an array of all the colors from the rainbow
// This function generates an array of all the colors from the rainbow


### Or use the Name of the function

```golang
func PrintRainbowColors() {

}
// or 
func DisplayHTTPErrors() {
    
}
// or
func GetUniqueId() string {
    
}
func GenerateUniqueId() string {

}
```

### Explain the code below

```
// Explain the code
```

// golang: generate a cli program using the Flags package with a string parameter --name and prints the value of name



// golang: implement a small cli program to generate and print a unique id


// golang: implement a small http server to display an html message

// golang: implement a small cli to display hello

